import LatLonSpherical from '/static/latlon-spherical.js';

class CMSMap {
    static map = null;
    static aisTracksLayerGroup = L.layerGroup(); 
    static ownShipTracksLayerGroup = L.layerGroup(); 
    static weaponTracksLayerGroup = L.layerGroup();
    static nauticalRoutesLayerGroup = L.layerGroup(); 
    static aisTracks = []; 
    static ownShipTracks = []; 
    static weaponTracks = [];
    static actors = [];
    static selectedTrack = null;
    static MIN_SPEED = 0;
    static OWNSHIP_MAX_SPEED = 100;
    static WEAPON_MAX_SPEED = 25000;
    static WEAPON_AUTOGUIDED_SENSIBILITY_AZIMUTH = 15;
    static WEAPON_AUTOGUIDED_GAIN_AZIMUTH = 3;
    static CMS_URL = window.origin;
    constructor(map) {
      CMSMap.map = map;
      CMSMap.map._layersMaxZoom = 19; 
      CMSMap.map.setView([0, 0], 2);
      CMSMap.map.setZoom(2);
      CMSMap.map.addLayer(CMSMap.aisTracksLayerGroup);
      CMSMap.map.addLayer(CMSMap.ownShipTracksLayerGroup);
      CMSMap.map.addLayer(CMSMap.weaponTracksLayerGroup);
      CMSMap.map.addLayer(CMSMap.nauticalRoutesLayerGroup);

      CMSMap.map.on('click', function(e) {
        document.getElementById ('start_ownship_lat').value = e.latlng.lat;
        document.getElementById ('start_ownship_lng').value = e.latlng.lng;
        document.getElementById ('start_ownship_id').value = Date.now();
        CMSMap.switchSelectedMarker();
        CMSMap.resetTrackDetails();
      });
      CMSMap.map.on('zoom', function() { });
    }
    get innerHTMLDetails() {
        var html = "";
        return html;
    }
    static buildMockedRoute(){
        let MOCK_ROUTE = [ [44.40855764369051, 8.926610356211857], [ 44.40961531290807 ,8.924077930910876], [44.40811772329271 ,8.917644095029537],
        [44.399826040824365 ,8.921321222381266], [44.3966222855623, 8.918766156948235], 
        [44.389700331473435 , 8.93977417960651], [44.3844353690267, 8.94018342113094]];
        let MOCK_ROUTE_POLYLINE = L.polyline(MOCK_ROUTE, { className: 'polyline_route' });
        CMSMap.nauticalRoutesLayerGroup.addLayer(MOCK_ROUTE_POLYLINE);
    }
    static getDistance(x1, y1, x2, y2){
        let y = x2 - x1;
        let x = y2 - y1;
        return Math.round(Math.sqrt(x * x + y * y));
    }
    static getRadiusNoise(map, ownShipTrack, threshold){
            //TODO: Implement realistic Absorption of sound in seawater formula according Enviroment System Component
            //TODO: For now, using the default alpha = 0.06 dB/km (0.00006 db/m)
            //TODO: Counterpart: Implement realistic passive sonar model formula : for now, using SNR (decibels) = SL – 2TL + TS – NL (basic sonar equation)
            //TODO: For basic sonar equation: SL = source level (dbNoise), TL (transmission loss: based on default absortion of sound and alpha above)
            //TODO: For now, TS (Target strengh) and Noise Level (NL) are ignored, reducing our Sonar formula to : SNR(db) = SL - 2TL.
            //TODO: The amount of SNR exceeds the detection threshold (DT) is called signal excess (SE) : SE(db) = SBR - DT. If SE > 0 we have a detection.
            //TODO: Since we are ignoring Noise Level for now, lets just assume SE(db) when SNR > 1.
            //TODO: In this context, the radius of circle marker (representing the point where the ship can be still detected) is just the value where SL - 2TL = 1
            //TODO: Therefore this.dbNoise - alpha * radius = 1  -> radius(km) = this.dbNoise/alpha
            var currentPos = new LatLonSpherical(ownShipTrack.latitude,ownShipTrack.longitude);
            var maxDistance = (ownShipTrack.dBNoise - threshold) /0.00006;
            var radiusLatLng  = currentPos.destinationPoint(maxDistance, 0); // Angle = 0 : Dont mind the angle we are just computing radius.
            var currentLatLongLeaflet = new L.LatLng(ownShipTrack.latitude, ownShipTrack.longitude);
            var currentZoom = map.getZoom();
            var projectedcurrentPos = map.project(currentLatLongLeaflet,currentZoom);
            var projectedRadiusPos = map.project(new L.LatLng(radiusLatLng.lat, radiusLatLng.lon),currentZoom);
            return CMSMap.getDistance(projectedcurrentPos.x, projectedcurrentPos.y, projectedRadiusPos.x, projectedRadiusPos.y);
    }

    static getRadiusDetection(map, ownShipTrack){
        var currentPos = new LatLonSpherical(ownShipTrack.latitude,ownShipTrack.longitude);
        var radiusLatLng  = currentPos.destinationPoint(ownShipTrack.detectionRange, 0); // Angle = 0 : Dont mind the angle we are just computing radius.
        var currentLatLongLeaflet = new L.LatLng(ownShipTrack.latitude, ownShipTrack.longitude);
        var currentZoom = map.getZoom();
        var projectedcurrentPos = map.project(currentLatLongLeaflet,currentZoom);
        var projectedRadiusPos = map.project(new L.LatLng(radiusLatLng.lat, radiusLatLng.lon),currentZoom);
        return CMSMap.getDistance(projectedcurrentPos.x, projectedcurrentPos.y, projectedRadiusPos.x, projectedRadiusPos.y);
}

    static async updateMap() {
        await CMSMap.loadActors();
        await CMSMap.buildAISTrackMarkers();
        CMSMap.renderAISTrackList();
        await CMSMap.buildWeaponMarkers();
        CMSMap.renderWeaponTrackList();
        await CMSMap.buildOwnShipMarkers();
        CMSMap.renderOnwShipList();
        if(typeof CMSMap.selectedTrack !== 'undefined' && CMSMap.selectedTrack != null ){
            CMSMap.renderTrackInfoArea(CMSMap.selectedTrack.marker); 
        }
        else{
            CMSMap.resetTrackDetails();
        }
        setTimeout(await CMSMap.updateMap(), 2000);
    }

    static updateSlider() {
        if(typeof CMSMap.selectedTrack !== 'undefined' && CMSMap.selectedTrack != null ){
            if(CMSMap.selectedTrack.tracktype != "aistrack" && CMSMap.selectedTrack.sliderCourse != null && CMSMap.selectedTrack.sliderSpeed != null){
                CMSMap.selectedTrack.sliderCourse.stepNo = CMSMap.selectedTrack.courseOverGround * 10;
                CMSMap.selectedTrack.sliderSpeed.stepNo = Math.floor(CMSMap.selectedTrack.speedOverGround * 10);
            }
        }
        setTimeout(CMSMap.updateSlider(), 10000);
    }

    static async buildAISTrackMarkers() {
        let jsonAISTracks = await CMSMap.loadAISTracks();
        // First try to locate a existing track in the new JSON message
        for (let i = 0; i < CMSMap.aisTracks.length; i++) {
            let aisTrack = jsonAISTracks.find(el => el.mmsi === CMSMap.aisTracks[i].mmsi);
            if (typeof aisTrack === 'undefined') {
                //If the previous existing track is not present, we shall remove it from map
                CMSMap.aisTracksLayerGroup.removeLayer(CMSMap.aisTracks[i].marker);
                // If the track lost were selected, deselect it
                if(CMSMap.aisTracks[i].selected){
                    CMSMap.aisTracks[i].selected = false;
                }
                CMSMap.selectedTrack = null;
                // And update the vector
                CMSMap.aisTracks.splice(i,1);
                i--;
            }
        }

        for (let i = 0; i < jsonAISTracks.length; i++) {
          let aisTrack = CMSMap.aisTracks.find(el => el.mmsi === jsonAISTracks[i].mmsi);
          if (typeof aisTrack !== 'undefined') {
            aisTrack.messageType = jsonAISTracks[i].messageType;
            aisTrack.navigationStatus = jsonAISTracks[i].navigationStatus;
            aisTrack.latitude = jsonAISTracks[i].latitude;
            aisTrack.longitude = jsonAISTracks[i].longitude;
            aisTrack.altitude = jsonAISTracks[i].altitude;
            aisTrack.courseOverGround =  jsonAISTracks[i].courseOverGround;
            aisTrack.speedOverGround = jsonAISTracks[i].speedOverGround;
            aisTrack.trueHeading = jsonAISTracks[i].trueHeading;
            aisTrack.rateOfTurn = jsonAISTracks[i].rateOfTurn;
            aisTrack.positionAccuracy = jsonAISTracks[i].positionAccuracy;
            aisTrack.specialManeuverIndicator = jsonAISTracks[i].specialManeuverIndicator;
            aisTrack.raimFlag = jsonAISTracks[i].raimFlag;
            aisTrack.communicationState = jsonAISTracks[i].communicationState;
            aisTrack.syncState = jsonAISTracks[i].syncState;
            aisTrack.slotIncrement = jsonAISTracks[i].slotIncrement;
            aisTrack.numberOfSlots = jsonAISTracks[i].numberOfSlots;
            aisTrack.keepFlag = jsonAISTracks[i].keepFlag;
            aisTrack.slotTimeout = jsonAISTracks[i].slotTimeout;
            aisTrack.numberOfReceivedStations = jsonAISTracks[i].numberOfReceivedStations;
            aisTrack.slotNumber = jsonAISTracks[i].slotNumber;
            aisTrack.utcHour = jsonAISTracks[i].utcHour;
            aisTrack.utcMinute = jsonAISTracks[i].utcMinute;
            aisTrack.slotOffset = jsonAISTracks[i].slotOffset;
            aisTrack.imo = jsonAISTracks[i].imo;
            aisTrack.callsign = jsonAISTracks[i].callsign;
            aisTrack.shipName = jsonAISTracks[i].shipName;
            aisTrack.shipType = jsonAISTracks[i].shipType;
            aisTrack.toBow = jsonAISTracks[i].toBow;
            aisTrack.toStern = jsonAISTracks[i].toStern;
            aisTrack.toStarboard = jsonAISTracks[i].toStarboard;
            aisTrack.toPort = jsonAISTracks[i].toPort;
            aisTrack.positionFixingDevice = jsonAISTracks[i].positionFixingDevice;
            aisTrack.destination = jsonAISTracks[i].destination;
            aisTrack.etaMonth = jsonAISTracks[i].etaMonth;
            aisTrack.etaDay = jsonAISTracks[i].etaDay;
            aisTrack.etaHour = jsonAISTracks[i].etaHour;
            aisTrack.etaMinute = jsonAISTracks[i].etaMinute;
            aisTrack.draught = jsonAISTracks[i].draught;
            aisTrack.dataTerminalReady = jsonAISTracks[i].dataTerminalReady;
            aisTrack.sequenceNumber = jsonAISTracks[i].sequenceNumber;
            aisTrack.destinationMmsi = jsonAISTracks[i].destinationMmsi;
            aisTrack.retransmit = jsonAISTracks[i].retransmit;
            aisTrack.spare = jsonAISTracks[i].spare;
            aisTrack.designatedAreaCode = jsonAISTracks[i].designatedAreaCode;
            aisTrack.functionalId = jsonAISTracks[i].functionalId;
            aisTrack.binaryData = jsonAISTracks[i].binaryData;
            aisTrack.applicationSpecificMessage = jsonAISTracks[i].applicationSpecificMessage;
            aisTrack.transponderClass = jsonAISTracks[i].transponderClass;
            aisTrack.valid = jsonAISTracks[i].valid;
            aisTrack.serverutc = jsonAISTracks[i].serverutc;
            aisTrack.repeatIndicator = jsonAISTracks[i].repeatIndicator;
            aisTrack.second = jsonAISTracks[i].second;
            let actor = CMSMap.actors.find(el => el.mmsi === aisTrack.mmsi);
            if (typeof actor !== 'undefined') {
              aisTrack.adbName = actor["fullName"];
              aisTrack.adbDimension = actor["dimension"];     
              aisTrack.adbType = actor["type"];    
              aisTrack.adbClassification = actor["classification"];        
            }
            aisTrack.marker.setIcon(aisTrack.icon);
            aisTrack.marker.setLatLng(new L.LatLng(aisTrack.latitude, aisTrack.longitude));
            if(aisTrack.messageType != "BASE_STATION_REPORT"){
              aisTrack.marker.setRotationAngle(aisTrack.courseOverGround - 90);
            }  
            //let latlongs = aisTrack.historyLine.getLatLngs();
            //latlongs.unshift(new L.LatLng(aisTrack.latitude, aisTrack.longitude));
            //aisTrack.historyLine.setLatLngs(latlongs);
            //aisTrack.circlemarker.setLatLng(new L.LatLng(aisTrack.latitude, aisTrack.longitude));
            //let radius = getRadiusNoise(aisTrack.latitude, aisTrack.longitude, aisTrack.dBNoise);
            //aisTrack.circlemarker.setRadius(radius);	
          }
          else{
            CMSMap.aisTracks[i] = new AISTrack.Builder(jsonAISTracks[i].mmsi)
            .withName(jsonAISTracks[i].mmsi).withMessageType(jsonAISTracks[i].messageType).withNavigationStatus(jsonAISTracks[i].navigationStatus)
            .withLatitude(jsonAISTracks[i].latitude).withLongitude(jsonAISTracks[i].longitude).withAltitude(jsonAISTracks[i].altitude)
            .withCoG(jsonAISTracks[i].courseOverGround).withSoG(jsonAISTracks[i].speedOverGround).withTrueHeading(jsonAISTracks[i].trueHeading)
            .withRateOfTurn(jsonAISTracks[i].rateOfTurn).withPositionAccuracy(jsonAISTracks[i].positionAccuracy)
            .withSpecialManeuverIndicator(jsonAISTracks[i].specialManeuverIndicator).withRaimFlag(jsonAISTracks[i].raimFlag)
            .withCommunicationState(jsonAISTracks[i].communicationState).withSyncState(jsonAISTracks[i].syncState).withSlotIncrement(jsonAISTracks[i].slotIncrement)
            .withNumberOfSlots(jsonAISTracks[i].numberOfSlots).withKeepFlag(jsonAISTracks[i].keepFlag).withSlotTimeout(jsonAISTracks[i].slotTimeout)
            .withNumberOfReceivedStations(jsonAISTracks[i].numberOfReceivedStations).withSlotNumber(jsonAISTracks[i].slotNumber).withSlotOffset(jsonAISTracks[i].slotOffset)
            .withUtcHour(jsonAISTracks[i].utcHour).withUtcMinute(jsonAISTracks[i].utcMinute).withIMO(jsonAISTracks[i].imo).withCallsign(jsonAISTracks[i].callsign)
            .withShipName(jsonAISTracks[i].shipName).withShipType(jsonAISTracks[i].shipType).withToBow(jsonAISTracks[i].toBow).withToStern(jsonAISTracks[i].toStern)
            .withToStarboard(jsonAISTracks[i].toStarboard).withToPort(jsonAISTracks[i].toPort).withPositionFixingDevice(jsonAISTracks[i].positionFixingDevice)
            .withDestination(jsonAISTracks[i].destination).withDestinationMmsi(jsonAISTracks[i].destinationMmsi).withDraught(jsonAISTracks[i].draught).withValid(jsonAISTracks[i].valid)
            .withETAMonth(jsonAISTracks[i].etaMonth).withETADay(jsonAISTracks[i].etaDay).withETAHour(jsonAISTracks[i].etaHour).withETAMinute(jsonAISTracks[i].etaMinute)
            .withDataTerminalReady(jsonAISTracks[i].dataTerminalReady).withSequenceNumber(jsonAISTracks[i].sequenceNumber).withRetransmit(jsonAISTracks[i].retransmit)
            .withTransponderClass(jsonAISTracks[i].transponderClass).withSpare(jsonAISTracks[i].spare).withDesignatedAreaCode(jsonAISTracks[i].designatedAreaCode)
            .withRepeatIndicator(jsonAISTracks[i].repeatIndicator).withFunctionalId(jsonAISTracks[i].functionalId).withBinaryData(jsonAISTracks[i].binaryData)
            .withApplicationSpecificMessage(jsonAISTracks[i].applicationSpecificMessage).withSecond(jsonAISTracks[i].second).withServerUTC(jsonAISTracks[i].serverutc)
            .build();
            let actor = CMSMap.actors.find(el => el.mmsi === CMSMap.aisTracks[i].mmsi);
            if (typeof actor !== 'undefined') {
                CMSMap.aisTracks[i].adbName = actor["fullName"];
                CMSMap.aisTracks[i].adbDimension = actor["dimension"];     
                CMSMap.aisTracks[i].adbType = actor["type"];    
                CMSMap.aisTracks[i].adbClassification = actor["classification"];        
            }
            if(CMSMap.aisTracks[i].messageType == "BASE_STATION_REPORT"){
                CMSMap.aisTracks[i].marker = L.marker([CMSMap.aisTracks[i].latitude,CMSMap.aisTracks[i].longitude],{icon: CMSMap.aisTracks[i].icon});
            }
            else{
                CMSMap.aisTracks[i].marker = L.marker([CMSMap.aisTracks[i].latitude,CMSMap.aisTracks[i].longitude],{icon: CMSMap.aisTracks[i].icon, rotationAngle: CMSMap.aisTracks[i].courseOverGround - 90});
            }
            CMSMap.aisTracks[i].marker.id = CMSMap.aisTracks[i].mmsi;
    
            /* TODO: Solve situation when track is idle to not add multiple repeated values.
            let polylineHistoryPoints = [];
            let historyData = await getOwnShipHistory(jsonOwnShips[i]);
            for (let j = 0; j < historyData.length; j++) {
              polylineHistoryPoints.push(new L.LatLng(historyData[j].latitude, historyData[j].longitude));
            }  
            ownShipTracks[i].historyLine = L.polyline(polylineHistoryPoints, { className: 'polyline2_history' }).addTo(map);
            */
            CMSMap.aisTracks[i].marker.on('click', CMSMap.onClickAISMarker);
            CMSMap.aisTracksLayerGroup.addLayer(CMSMap.aisTracks[i].marker);
        }
      }
    }

    static async buildOwnShipMarkers() {
        let jsonOwnShips = await CMSMap.loadOwnShips();
        // First try to locate a existing track in the new JSON message
        for (let i = 0; i < CMSMap.ownShipTracks.length; i++) {
            let ownShipTrack = jsonOwnShips.find(el => el.mmsi === CMSMap.ownShipTracks[i].mmsi);
            if (typeof ownShipTrack === 'undefined') {
                //If the previous existing track is not present, we shall remove it from map
                CMSMap.ownShipTracksLayerGroup.removeLayer(CMSMap.ownShipTracks[i].marker);
                CMSMap.ownShipTracksLayerGroup.removeLayer(CMSMap.ownShipTracks[i].historyLine);

                CMSMap.ownShipTracks[i].circlemarker.remove(CMSMap.map);
                CMSMap.ownShipTracks[i].circleDetectionmarker.remove(CMSMap.map);
                // If the track lost were selected, deselect it
                if(CMSMap.ownShipTracks[i].selected){
                    CMSMap.ownShipTracks[i].selected = false;
                }
                CMSMap.selectedTrack = null;
                // And update the vector
                CMSMap.ownShipTracks.splice(i,1);
                i--;
            }
        }
        // Now we remove the trash, we process the other tracks (shall already exist or be created)
        for (let i = 0; i < jsonOwnShips.length; i++) {
          let html = ``;
          let ownShipTrack = CMSMap.ownShipTracks.find(el => el.mmsi === jsonOwnShips[i].mmsi);
          if (typeof ownShipTrack !== 'undefined') {
            ownShipTrack.serverutc = jsonOwnShips[i].serverutc;
            ownShipTrack.latitude = jsonOwnShips[i].latitude;
            ownShipTrack.longitude = jsonOwnShips[i].longitude;
            ownShipTrack.courseOverGround = jsonOwnShips[i].courseOverGround;
            ownShipTrack.speedOverGround = jsonOwnShips[i].speedOverGround;
            ownShipTrack.dBNoise = jsonOwnShips[i].dBNoise;
            ownShipTrack.fuelInLiters = jsonOwnShips[i].fuelInLiters;
            ownShipTrack.hit = jsonOwnShips[i].hit;
            ownShipTrack.marker.setIcon(ownShipTrack.icon);
            ownShipTrack.marker.setLatLng(new L.LatLng(ownShipTrack.latitude, ownShipTrack.longitude));
            ownShipTrack.marker.setRotationAngle(ownShipTrack.courseOverGround - 90);
            let latlongs = ownShipTrack.historyLine.getLatLngs();
            let currentLatLong = new L.LatLng(ownShipTrack.latitude, ownShipTrack.longitude)
            latlongs.unshift(currentLatLong);
            ownShipTrack.historyLine.setLatLngs(latlongs);
            //var currentPos = new LatLonSpherical(ownShipTrack.latitude,ownShipTrack.longitude);
            //var distance = speedSI * ownShipTrack.projectionPeriod * 1;
            //ownShipTrack.projectionPoint  = currentPos.destinationPoint(distance, ownShipTrack.courseOverGround);
            //latlongs = ownShipTrack.projectionLine.getLatLngs();
            //latlongs.pop();
            //latlongs.pop();
            //latlongs.push(currentLatLong);
            //latlongs.push(new L.LatLng(ownShipTrack.projectionPoint.lat, ownShipTrack.projectionPoint.lon));
            //ownShipTrack.projectionLine.setLatLngs(latlongs);
            if(!ownShipTrack.hit){
                await Track.loadDetections(ownShipTrack);
                ownShipTrack.circlemarker.setLatLng(new L.LatLng(ownShipTrack.latitude, ownShipTrack.longitude));
                let radiusNoise = CMSMap.getRadiusNoise(CMSMap.map, ownShipTrack, 0);
                ownShipTrack.circlemarker.setRadius(radiusNoise);
                let radiusDetection = CMSMap.getRadiusDetection(CMSMap.map, ownShipTrack);	
                ownShipTrack.circleDetectionmarker.setLatLng(new L.LatLng(ownShipTrack.latitude, ownShipTrack.longitude));
                ownShipTrack.circleDetectionmarker.setRadius(radiusDetection);
                ownShipTrack.circleDetectionmarker.setDirection(ownShipTrack.courseOverGround, ownShipTrack.detectionAzimuthRange);
            }
            else{
                ownShipTrack.detection = [];
                if(ownShipTrack.circlemarker != null){
                    ownShipTrack.circlemarker.remove(CMSMap.map);
                    ownShipTrack.circlemarker = null;
                }
                if(ownShipTrack.circleDetectionmarker != null){
                    ownShipTrack.circleDetectionmarker.remove(CMSMap.map);
                    ownShipTrack.circleDetectionmarker = null;
                }
            }
          }
          else{
            CMSMap.ownShipTracks[i] = new OwnShip.Builder(jsonOwnShips[i].mmsi)
            .withName(jsonOwnShips[i].mmsi)
            .withLatitude(jsonOwnShips[i].latitude)
            .withLongitude(jsonOwnShips[i].longitude)
            .withCoG(jsonOwnShips[i].courseOverGround)
            .withSoG(jsonOwnShips[i].speedOverGround)
            .withFuelInLiters(jsonOwnShips[i].fuelInLiters)
            .withNoiseInDb(jsonOwnShips[i].dBNoise)
            .withDetectionRange(jsonOwnShips[i].detectionRange)
            .withDetectionAzimuthRange(jsonOwnShips[i].detectionAzimuthRange)
            .withHit(jsonOwnShips[i].hit)
            .withServerUTC(jsonOwnShips[i].serverutc)
            .build();
            CMSMap.ownShipTracks[i].marker = L.marker([CMSMap.ownShipTracks[i].latitude,CMSMap.ownShipTracks[i].longitude],{icon: CMSMap.ownShipTracks[i].icon, rotationAngle: CMSMap.ownShipTracks[i].courseOverGround - 90});
            CMSMap.ownShipTracks[i].marker.id = CMSMap.ownShipTracks[i].mmsi;
            /*
              var currentPos = new LatLonSpherical(ownShipTracks[i].latitude,ownShipTracks[i].longitude);
              var speedSI  = 0.514444 * ownShipTracks[i].speedOverGround; 
              var distance = speedSI * ownShipTracks[i].projectionPeriod;
              ownShipTracks[i].projectionPoint  = currentPos.destinationPoint(distance, ownShipTracks[i].courseOverGround);
              var polylinePoints = [ [ownShipTracks[i].latitude,ownShipTracks[i].longitude] , [ownShipTracks[i].projectionPoint.lat,ownShipTracks[i].projectionPoint.lon] ];
              ownShipTracks[i].projectionLine = L.polyline(polylinePoints, { className: 'polyline_projection' }).setText(ownShipTracks[i].projectionText, {center: true}).addTo(map);
              */
            let polylineHistoryPoints = [];
            let historyData = await CMSMap.getOwnShipHistory(jsonOwnShips[i].mmsi);
            for (let j = 0; j < historyData.length; j++) {
              polylineHistoryPoints.push(new L.LatLng(historyData[j].latitude, historyData[j].longitude));
            }  
            CMSMap.ownShipTracks[i].historyLine = L.polyline(polylineHistoryPoints, { className: 'polyline_history' });
            CMSMap.ownShipTracks[i].historyLine.id = CMSMap.ownShipTracks[i].mmsi;
            
            if(!CMSMap.ownShipTracks[i].hit){
                await Track.loadDetections(CMSMap.ownShipTracks[i]);
                let radiusNoise= CMSMap.getRadiusNoise(CMSMap.map, CMSMap.ownShipTracks[i], 0);
                CMSMap.ownShipTracks[i].circlemarker =  L.circleMarker([CMSMap.ownShipTracks[i].latitude,CMSMap.ownShipTracks[i].longitude], {
                  "radius": radiusNoise, "fillColor": "#ff7800", "color": "#ff7800", "weight": 1, "opacity": 0.4}).addTo(CMSMap.map);
                let radiusDetection = CMSMap.getRadiusDetection(CMSMap.map, CMSMap.ownShipTracks[i]);
                CMSMap.ownShipTracks[i].circleDetectionmarker =  L.semiCircleMarker([CMSMap.ownShipTracks[i].latitude,CMSMap.ownShipTracks[i].longitude], {
                    "radius": radiusDetection, "color": "#0077ff", "weight": 2, "opacity": 0.5}).setDirection(CMSMap.ownShipTracks[i].courseOverGround, CMSMap.ownShipTracks[i].detectionAzimuthRange).addTo(CMSMap.map);
            }
            CMSMap.ownShipTracks[i].marker.on('click', CMSMap.onClickMarker);
            CMSMap.ownShipTracks[i].historyLine.on('click', CMSMap.onClickMarker);
            CMSMap.ownShipTracksLayerGroup.addLayer(CMSMap.ownShipTracks[i].marker);
            CMSMap.ownShipTracksLayerGroup.addLayer(CMSMap.ownShipTracks[i].historyLine);
          }
        }
    }

    static async buildWeaponMarkers() {
        let jsonWeapons = await CMSMap.loadWeapons();
        // First try to locate a existing track in the new JSON structure
        for (let i = 0; i < CMSMap.weaponTracks.length; i++) {
            let weaponTrack = jsonWeapons.find(el => el.wid === CMSMap.weaponTracks[i].wid);
            if (typeof weaponTrack === 'undefined') {
                //If the previous existing track is not present, we shall remove it from map
                CMSMap.weaponTracksLayerGroup.removeLayer(CMSMap.weaponTracks[i].marker);
                CMSMap.weaponTracksLayerGroup.removeLayer(CMSMap.weaponTracks[i].historyLine);
                CMSMap.weaponTracks[i].circleDetectionmarker.remove(CMSMap.map);
                CMSMap.weaponTracks[i].circleAutoGuidedDetectionMarker.remove(CMSMap.map);
                // If the track lost were selected, deselect it
                if(CMSMap.weaponTracks[i].selected){
                    CMSMap.weaponTracks[i].selected = false;
                    CMSMap.selectedTrack = null;
                }
                // And update the vector
                CMSMap.weaponTracks.splice(i,1);
                i--;
            }
        }
        // Now we remove the trash, we process the other tracks (shall already exist or be created)
        for (let i = 0; i < jsonWeapons.length; i++) {
          let html = ``;
          let weaponTrack = CMSMap.weaponTracks.find(el => el.wid === jsonWeapons[i].wid);
          if (typeof weaponTrack !== 'undefined') {
            weaponTrack.serverutc = jsonWeapons[i].serverutc;             
            weaponTrack.latitude = jsonWeapons[i].latitude;
            weaponTrack.longitude = jsonWeapons[i].longitude;
            weaponTrack.courseOverGround = jsonWeapons[i].courseOverGround;
            weaponTrack.speedOverGround = jsonWeapons[i].speedOverGround;
            weaponTrack.distanceTravelled = jsonWeapons[i].distanceTravelled; 
            weaponTrack.dBNoise = jsonWeapons[i].dBNoise;
            weaponTrack.fuelInLiters = jsonWeapons[i].fuelInLiters;
            await Track.loadDetections(weaponTrack);
            weaponTrack.marker.setLatLng(new L.LatLng(weaponTrack.latitude, weaponTrack.longitude));
            weaponTrack.marker.setRotationAngle(weaponTrack.courseOverGround - 90);
            let latlongs = weaponTrack.historyLine.getLatLngs();
            let currentLatLong = new L.LatLng(weaponTrack.latitude, weaponTrack.longitude)
            latlongs.unshift(currentLatLong);
            weaponTrack.historyLine.setLatLngs(latlongs);

            let radiusDetection = CMSMap.getRadiusDetection(CMSMap.map, weaponTrack);	
            weaponTrack.circleDetectionmarker.setLatLng(new L.LatLng(weaponTrack.latitude, weaponTrack.longitude));
            weaponTrack.circleDetectionmarker.setRadius(radiusDetection);
            weaponTrack.circleDetectionmarker.setDirection(weaponTrack.courseOverGround, weaponTrack.detectionAzimuthRange);
            weaponTrack.circleAutoGuidedDetectionMarker.setLatLng(new L.LatLng(weaponTrack.latitude, weaponTrack.longitude));
            weaponTrack.circleAutoGuidedDetectionMarker.setRadius(this.WEAPON_AUTOGUIDED_GAIN_AZIMUTH * radiusDetection);
            weaponTrack.circleAutoGuidedDetectionMarker.setDirection(weaponTrack.courseOverGround, this.WEAPON_AUTOGUIDED_SENSIBILITY_AZIMUTH);
          }
          else{
            CMSMap.weaponTracks[i] = new Weapon.Builder(jsonWeapons[i].wid)
            .withName(jsonWeapons[i].wid)
            .withLatitude(jsonWeapons[i].latitude)
            .withLongitude(jsonWeapons[i].longitude)
            .withCoG(jsonWeapons[i].courseOverGround)
            .withSoG(jsonWeapons[i].speedOverGround)
            .withDistanceTravelled(jsonWeapons[i].distanceTravelled)
            .withFuelInLiters(jsonWeapons[i].fuelInLiters)
            .withNoiseInDb(jsonWeapons[i].dBNoise)
            .withDetectionRange(jsonWeapons[i].detectionRange)
            .withDetectionAzimuthRange(jsonWeapons[i].detectionAzimuthRange)
            .withServerUTC(jsonWeapons[i].serverutc)
            .build();
            await Track.loadDetections(CMSMap.weaponTracks[i]);
            CMSMap.weaponTracks[i].marker = L.marker([CMSMap.weaponTracks[i].latitude,CMSMap.weaponTracks[i].longitude],
                {icon: CMSMap.weaponTracks[i].icon, rotationAngle: CMSMap.weaponTracks[i].courseOverGround - 90});
            CMSMap.weaponTracks[i].marker.id = CMSMap.weaponTracks[i].wid;
            let polylineHistoryPoints = [];
            let historyData = await CMSMap.getWeaponHistory(jsonWeapons[i].wid);
            for (let j = 0; j < historyData.length; j++) {
              polylineHistoryPoints.push(new L.LatLng(historyData[j].latitude, historyData[j].longitude));
            }  
            CMSMap.weaponTracks[i].historyLine = L.polyline(polylineHistoryPoints, { className: 'polyline_history_weapon' });
            CMSMap.weaponTracks[i].historyLine.id = CMSMap.weaponTracks[i].wid;
            CMSMap.weaponTracks[i].marker.on('click', CMSMap.onClickWeaponMarker);
            CMSMap.weaponTracks[i].historyLine.on('click', CMSMap.onClickWeaponMarker);
            let radiusDetection = CMSMap.getRadiusDetection(CMSMap.map, CMSMap.weaponTracks[i]);
            CMSMap.weaponTracks[i].circleDetectionmarker =  L.semiCircleMarker([CMSMap.weaponTracks[i].latitude,CMSMap.weaponTracks[i].longitude], {
                "radius": radiusDetection, "color": "#0077ff", "weight": 2, "opacity": 0.5})
                .setDirection(CMSMap.weaponTracks[i].courseOverGround, CMSMap.weaponTracks[i].detectionAzimuthRange).addTo(CMSMap.map);
            CMSMap.weaponTracks[i].circleAutoGuidedDetectionMarker = L.semiCircleMarker([CMSMap.weaponTracks[i].latitude,CMSMap.weaponTracks[i].longitude], {
                "radius": this.WEAPON_AUTOGUIDED_GAIN_AZIMUTH * radiusDetection, "color": "#dd0000", "weight": 2, "opacity": 0.5})
                .setDirection(CMSMap.weaponTracks[i].courseOverGround, this.WEAPON_AUTOGUIDED_SENSIBILITY_AZIMUTH).addTo(CMSMap.map);
            CMSMap.weaponTracksLayerGroup.addLayer(CMSMap.weaponTracks[i].marker);
            CMSMap.weaponTracksLayerGroup.addLayer(CMSMap.weaponTracks[i].historyLine);
          }
        }
    }

    static onClickMarker(e){
        CMSMap.map.setView([e.latlng.lat, e.latlng.lng], CMSMap.map.getZoom());
        CMSMap.switchSelectedMarker(e.target);
        CMSMap.renderOnwShipList(); 
        CMSMap.renderTrackInfoArea(e.target);
        CMSMap.renderTrackControlArea(e.target);
    }

    static onClickWeaponMarker(e){
        CMSMap.map.setView([e.latlng.lat, e.latlng.lng], CMSMap.map.getZoom());
        CMSMap.switchSelectedMarker(e.target);
        CMSMap.renderWeaponTrackList(); 
        CMSMap.renderTrackInfoArea(e.target);
        CMSMap.renderTrackControlArea(e.target);
    }

    static onClickAISMarker(e){
        CMSMap.map.setView([e.latlng.lat, e.latlng.lng], CMSMap.map.getZoom());
        CMSMap.switchSelectedMarker(e.target);
        CMSMap.renderAISTrackList(); 
        CMSMap.renderTrackInfoArea(e.target);
        CMSMap.renderTrackControlArea(e.target);
    }

    static switchSelectedMarker(trackMarker){
        //If trackMarker already is the selectedTrack, nothing to do
        if(CMSMap.selectedTrack != null && typeof CMSMap.selectedTrack !== 'undefined' 
            && typeof trackMarker !== 'undefined' && CMSMap.selectedTrack.mmsi == trackMarker.id){
            return;
        }

        // I) Find the last selected to unselect it
        if(typeof CMSMap.selectedTrack !== 'undefined' && CMSMap.selectedTrack != null ){
            // 1st try find it at the ownShipTracks list
            let ownShipTrack = CMSMap.ownShipTracks.find(el => el.mmsi === CMSMap.selectedTrack.mmsi);
            if(typeof ownShipTrack !== 'undefined'){
                ownShipTrack.selected = false;
                ownShipTrack.marker.setIcon(ownShipTrack.icon);
                CMSMap.ownShipTracksLayerGroup.removeLayer(ownShipTrack.historyLine);
                ownShipTrack.historyLine.setStyle({ className: 'polyline_history' });
                CMSMap.ownShipTracksLayerGroup.addLayer(ownShipTrack.historyLine);
            }

            // 2nd try find it at the aisTracks list
            let aisTrack = CMSMap.aisTracks.find(el => el.mmsi === CMSMap.selectedTrack.mmsi);
            if(typeof aisTrack !== 'undefined'){
                aisTrack.selected = false;
            }
            // 3rd try find it at the weaponTracks list
            let weaponTrack = CMSMap.weaponTracks.find(el => el.wid === CMSMap.selectedTrack.mmsi);
            if(typeof weaponTrack !== 'undefined'){
                weaponTrack.selected = false;
                CMSMap.weaponTracksLayerGroup.removeLayer(weaponTrack.historyLine);
                weaponTrack.historyLine.setStyle({ className: 'polyline_history_weapon' });
                CMSMap.weaponTracksLayerGroup.addLayer(weaponTrack.historyLine);
            }
        }
        // II) Find the target marker to select it
        if(typeof trackMarker !== 'undefined'){
            // 1st try find it at the ownShipTracks list
            let ownShipTrack = CMSMap.ownShipTracks.find(el => el.mmsi === trackMarker.id);
            if(typeof ownShipTrack !== 'undefined'){
                CMSMap.selectedTrack = ownShipTrack;
                CMSMap.selectedTrack.selected = true;
                CMSMap.selectedTrack.marker.setIcon(CMSMap.selectedTrack.icon);
                CMSMap.ownShipTracksLayerGroup.removeLayer(CMSMap.selectedTrack.historyLine);
                CMSMap.selectedTrack.historyLine.setStyle({ className: 'polyline_history_selected' });
                CMSMap.ownShipTracksLayerGroup.addLayer(CMSMap.selectedTrack.historyLine);
            }
            // 2nd try find it at the aisTracks list
            let aisTrack = CMSMap.aisTracks.find(el => el.mmsi === trackMarker.id);
            if(typeof aisTrack !== 'undefined'){
                CMSMap.selectedTrack = aisTrack;
                CMSMap.selectedTrack.selected = true;
                CMSMap.selectedTrack.marker.setIcon(CMSMap.selectedTrack.icon);

            }
            // 3rd try find it at the weaponTracks list
            let weaponTrack = CMSMap.weaponTracks.find(el => el.wid === trackMarker.id);
            if(typeof weaponTrack !== 'undefined'){
                CMSMap.selectedTrack = weaponTrack;
                CMSMap.selectedTrack.selected = true;
                CMSMap.selectedTrack.marker.setIcon(CMSMap.selectedTrack.icon);
                CMSMap.weaponTracksLayerGroup.removeLayer(CMSMap.selectedTrack.historyLine);
                CMSMap.selectedTrack.historyLine.setStyle({ className: 'polyline_history_weapon_selected' });
                CMSMap.weaponTracksLayerGroup.addLayer(CMSMap.selectedTrack.historyLine);
            }
        }
        // III) If there is no target, just reset global selected track
        else{
            CMSMap.selectedTrack = null;
        }
    }
    static renderWeaponTrackList(){
        let html = ``;
        let trackName = "";
        let linkcolor = "gray";
        for (let i = 0; i < CMSMap.weaponTracks.length; i++) {
          trackName = CMSMap.weaponTracks[i].wid;
          let elementId = 'select_'+CMSMap.weaponTracks[i].wid;
          let htmlSegment = `<a href="#" title="${CMSMap.weaponTracks[i].wid}" id="${elementId}" style="color: `+linkcolor+`">`;
          if(CMSMap.weaponTracks[i].selected){
            htmlSegment += `<strong>${trackName}</strong></a><br>`;
          }
          else{
            htmlSegment += `${trackName}</a><br>`;
          } 
          html += htmlSegment; 
        }
        let weaponTrackslist = document.getElementById('weaponTrackslist');
        weaponTrackslist.innerHTML = html;
        for (let i = 0; i < CMSMap.weaponTracks.length; i++) {
            let elementId = 'select_'+CMSMap.weaponTracks[i].mmsi;
            let myElement = document.getElementById(elementId);
            myElement.addEventListener("click", CMSMap.focusWeaponTrack,false);
            myElement.id = CMSMap.weaponTracks[i].wid;
        }

    }
    static renderAISTrackList(){
        let html = "";
        for (let i = 0; i < CMSMap.aisTracks.length; i++) {
          let trackName = "";
          let linkcolor = "gray";
          if(CMSMap.aisTracks[i].adbName != null){
            trackName = CMSMap.aisTracks[i].adbName;
          }
          else {
            trackName = CMSMap.aisTracks[i].mmsi;
          }
          if (typeof CMSMap.aisTracks[i].adbClassification !== 'undefined') {
              if(CMSMap.aisTracks[i].adbClassification == "FRIEND"){
                linkcolor = "blue";
              }
              if(CMSMap.aisTracks[i].adbClassification == "HOSTILE"){
                linkcolor = "red";
              }
          }
          let elementId = "select_"+CMSMap.aisTracks[i].mmsi;
          let htmlSegment = `<a href="#" title="${CMSMap.aisTracks[i].mmsi}" id="${elementId}" style="color: `+linkcolor+`">`;
          if(CMSMap.aisTracks[i].selected){
            htmlSegment += `<strong>${trackName}</strong></a><br>`;
          }
          else{
            htmlSegment += `${trackName}</a><br>`;
          }  
          html += htmlSegment;
        }
        let aisTrackslist = document.getElementById('aisTrackslist');
        aisTrackslist.innerHTML = html;
        for (let i = 0; i < CMSMap.aisTracks.length; i++) {
            let elementId = 'select_'+CMSMap.aisTracks[i].mmsi;
            let myElement = document.getElementById(elementId);
            myElement.addEventListener("click", CMSMap.focusAISTrack,false);
            myElement.id = CMSMap.aisTracks[i].mmsi;
        }
    }
    static renderOnwShipList() {
        let html = "";
        for (let i = 0; i < CMSMap.ownShipTracks.length; i++) {
            let trackName = "";
            let linkcolor = "black";
            let decoration = "initial";
            trackName = CMSMap.ownShipTracks[i].mmsi;
            let elementId = 'select_'+CMSMap.ownShipTracks[i].mmsi;
            if (typeof CMSMap.ownShipTracks[i].hit !== 'undefined') {
                if(CMSMap.ownShipTracks[i].hit){
                    //linkcolor = "red";
                    decoration = "line-through";
                }
            }
            let htmlSegment = `<a href="#" title="${CMSMap.ownShipTracks[i].mmsi}" id="${elementId}" style="text-decoration: `+decoration+`; color: `+linkcolor+`">`;
            if(CMSMap.ownShipTracks[i].selected){
                htmlSegment += `<strong>${trackName}</strong></a><br>`;
            }
            else{
                htmlSegment += `${trackName}</a><br>`;
            } 
            html += htmlSegment; 
        }
        let ownShipTrackslist = document.getElementById('ownShipTrackslist');
        ownShipTrackslist.innerHTML = html;
        for (let i = 0; i < CMSMap.ownShipTracks.length; i++) {
            let elementId = 'select_'+CMSMap.ownShipTracks[i].mmsi;
            let myElement = document.getElementById(elementId);
            myElement.addEventListener("click", CMSMap.focusOwnShip,false);
            myElement.id = CMSMap.ownShipTracks[i].mmsi;
        }
    }

    static renderTrackInfoArea(trackMarker) {
        let trackdetailsElement = document.getElementById('trackdetails');
        let html = "";
        if(typeof trackMarker === 'undefined'){
          html += `<p class="trackdetails"><strong> Track Info Area:</strong><hr>Please Select a Track</p>`;
        }
        if(typeof trackMarker !== 'undefined' && typeof trackMarker.id !== 'undefined'){
          html += `<p class="trackdetails"><strong> Track Info Area:</strong><hr>`;
          let ownShipTrack = CMSMap.ownShipTracks.find(el => el.marker.id === trackMarker.id);
            if(typeof ownShipTrack !== 'undefined'){
              html += ownShipTrack.innerHTMLDetails;
            }
            else{
              let aisTrack = CMSMap.aisTracks.find(el => el.marker.id === trackMarker.id);
              if(typeof aisTrack !== 'undefined'){
                html += aisTrack.innerHTMLDetails;
              }
              else {
                let weaponTrack = CMSMap.weaponTracks.find(el => el.marker.id === trackMarker.id);
                if(typeof weaponTrack !== 'undefined'){
                    html += weaponTrack.innerHTMLDetails;
                }
              }
            }
        }
        html += "</p>";
        trackdetailsElement.innerHTML = html;
      }
    
    static renderTrackControlArea(trackMarker) {
        let html = "<strong>Track Control Area:</strong><hr>";
        html += `<a href='#' id='deleteTrackUrl'>DELETE TRACK</a><br>`;
        let track = [];
        let trackcontrolarea = document.getElementById('trackcontrolarea');
        track = CMSMap.ownShipTracks.find(el => el.marker.id === trackMarker.id);
        if(typeof track === 'undefined'){
            track = CMSMap.weaponTracks.find(el => el.marker.id === trackMarker.id);
        }
        if(typeof track !== 'undefined'){
            if(!track.hit){
                html += `<a href='#' id='fireWeapon'>FIRE WEAPON !!!</a><br><br>`;
                html += `<span id="tip">Adjust dial to set Course and Speed.</span> 
                  <div id="legend">
                      <ul>
                          <li id="course"><span class="title"><span id="purple">Course</span>Course</span></li>
                          <li id="speed"><span class="title"><span id="blue">Speed</span>Speed</span></li>
                      </ul>
                      </div>`;
            }
        }
        else{
            track = CMSMap.aisTracks.find(el => el.marker.id === trackMarker.id);    
        }
        trackcontrolarea.innerHTML = html ;
        let myElement = document.getElementById("deleteTrackUrl");
        myElement.addEventListener("click", CMSMap.deleteTrackOnClick,false);
        myElement.id = track.mmsi;
        if(track.tracktype != 'aistrack' && !track.hit ){
            let maxspeed = 0;
            if(track.tracktype == 'ownship'){
                maxspeed = this.OWNSHIP_MAX_SPEED;
            }
            else if(track.tracktype == 'weapon'){
                maxspeed = this.WEAPON_MAX_SPEED;
            }


            track.sliderCourse =  new CircularSlider({container: 'trackcontrolarea', color: "#5d3b6d", max: 3599, min: 0, step: 1, radius: 190, 
            valueChange: val => {
                if(track.tracktype == 'ownship'){
                    //track.sliderCourse.stepNo = track.courseOverGround * 10;
                    CMSMap.setOwnShipCourseOverGround(track.mmsi,val);
                    //ownShipTrack.CoG = val;   
                }
            }});

            track.sliderSpeed = new CircularSlider({container: 'trackcontrolarea', color: "#127fc3", min: this.MIN_SPEED, max: 10 * maxspeed, step: 1, radius: 160, 
            valueChange: val => { 
                if(track.tracktype == 'ownship'){
                     //ownShipTrack.SoG = val;
                    //track.sliderSpeed.stepNo = Math.floor(track.speedOverGround * 10);
                    CMSMap.setOwnShipSpeedOverGround(track.mmsi,val);
                }
            }});

            track.sliderCourse.stepNo = track.courseOverGround * 10;
            track.sliderSpeed.stepNo = Math.floor(track.speedOverGround * 10);


            let myFireElement = document.getElementById("fireWeapon");
            myFireElement.addEventListener("click", CMSMap.fireWeaponOnClick,false);
            myFireElement.id = track.mmsi;
        }
        else{
            track.sliderCourse = null;
            track.sliderSpeed = null;
        }     
    }

    static focusOwnShip(evt) {
          let track = CMSMap.ownShipTracks.find(el => el.mmsi === parseInt(evt.currentTarget.id));
          CMSMap.map.setView([track.latitude, track.longitude], CMSMap.map.getZoom());
          CMSMap.switchSelectedMarker(track.marker);
          CMSMap.renderOnwShipList(); 
          CMSMap.renderTrackInfoArea(track.marker);
          CMSMap.renderTrackControlArea(track.marker);
    }

    static focusAISTrack(evt) {
        let track = CMSMap.aisTracks.find(el => el.mmsi === parseInt(evt.currentTarget.id));
        if(typeof track !== 'undefined'){
          CMSMap.map.setView([track.latitude, track.longitude], CMSMap.map.getZoom());
          CMSMap.switchSelectedMarker(track.marker);
          CMSMap.renderAISTrackList(); 
          CMSMap.renderTrackInfoArea(track.marker);
          CMSMap.renderTrackControlArea(track.marker);
        }
  }

    static focusWeaponTrack(evt) {
    let track = CMSMap.weaponTracks.find(el => el.mmsi === evt.currentTarget.id);
     if(typeof track !== 'undefined'){
        CMSMap.map.setView([track.latitude, track.longitude], CMSMap.map.getZoom());
        CMSMap.switchSelectedMarker(track.marker);
        CMSMap.renderWeaponTrackList(); 
        CMSMap.renderTrackInfoArea(track.marker);
        CMSMap.renderTrackControlArea(track.marker);
       }
    }
    
    static deleteTrackOnClick(evt) {
        let track = CMSMap.weaponTracks.find(el => el.mmsi === evt.currentTarget.id);    
        if(typeof track !== 'undefined'){
            CMSMap.deleteWeaponTrack(evt.currentTarget.id);
        }
        else{
            let id = parseInt(evt.currentTarget.id);
            track = CMSMap.ownShipTracks.find(el => el.mmsi === parseInt(evt.currentTarget.id));
            if(typeof track !== 'undefined'){
                CMSMap.deleteOwnShipTrack(id);
            }
            else{
                track = CMSMap.aisTracks.find(el => el.mmsi === parseInt(evt.currentTarget.id));
                if(typeof track !== 'undefined'){
                    CMSMap.deleteAISTrack(id);
                } 
            }
        }
        CMSMap.switchSelectedMarker();
        CMSMap.resetTrackDetails();
    }

    static fireWeaponOnClick(evt) {
        //TODO: Implement multitorpille.
        let id = parseInt(evt.currentTarget.id);
        let track = CMSMap.ownShipTracks.find(el => el.mmsi === id);
        if(typeof track !== 'undefined'){
            CMSMap.launchWeapon(id);
        }
    }


    static resetTrackDetails() {
        let trackdetailsElement = document.getElementById('trackdetails');
        trackdetailsElement.innerHTML = `<p class="trackdetails"><strong> Track Info Area:</strong><hr>Please Select a Track</p>`;
        let trackcontrolarea = document.getElementById('trackcontrolarea');
        trackcontrolarea.innerHTML = `<p class="trackcontrolarea"><strong> Track Control Area:</strong><hr>Please Select a Track</p>`;
    }

    static setOwnShipSpeedOverGround(id,speed) {
        let url = `${CMSMap.CMS_URL}/${id}/sog/${speed}`;
        try {
            fetch(url);
        } catch (error) {
            console.log(error);
        }
    }
    
    static setOwnShipCourseOverGround(id,course) {
        let url = `${CMSMap.CMS_URL}/${id}/cog/${course}`;
        try {
            fetch(url);
        } catch (error) {
            console.log(error);
        }
    }

    static async loadActors() {
        let url = `${CMSMap.CMS_URL}/actors`;
        try {
            let res = await fetch(url);
            CMSMap.actors = await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    static async loadAISTracks() {
        let url = `${CMSMap.CMS_URL}/aistracks`;
        try {
            let res = await fetch(url);
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }
    
    static async loadOwnShips() {
        let url = `${CMSMap.CMS_URL}/ownships`;
        try {
            let res = await fetch(url);
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    static async loadWeapons() {
        let url = `${CMSMap.CMS_URL}/weapons`;
        try {
            let res = await fetch(url);
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }

    static async getOwnShipHistory(id) {
        let url = `${CMSMap.CMS_URL}/${id}/history`;
        try {
            let res = await fetch(url);
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    }   

    static async getWeaponHistory(id) {
        let url = `${CMSMap.CMS_URL}/${id}/weapontrail`;
        try {
            let res = await fetch(url);
            return await res.json();
        } catch (error) {
            console.log(error);
        }
    } 

    static async deleteOwnShipTrack(id) {
        let url = `${CMSMap.CMS_URL}/${id}/deleteOwnShip`;
        try {
            let res = await fetch(url);
            console.log(res);
        } catch (error) {
            console.log(error);
        }
    }

    static async deleteAISTrack(id) {
        let url = `${CMSMap.CMS_URL}/${id}/del`;
        try {
            let res = await fetch(url);
            console.log(res);
        } catch (error) {
            console.log(error);
        }
    }

    static async deleteWeaponTrack(id) {
        let url = `${CMSMap.CMS_URL}/${id}/abortfire`;
        try {
            let res = await fetch(url);
            console.log(res);
        } catch (error) {
            console.log(error);
        }
    }

    static launchWeapon(id) {
        let url = `${CMSMap.CMS_URL}/${id}/fire`;
        try {
            fetch(url);
        } catch (error) {
            console.log(error);
        }
    }
    
  }

  export { CMSMap as default};