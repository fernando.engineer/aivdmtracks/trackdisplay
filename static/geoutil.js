
/*
 Other Utilities functions!
*/

function initGeolocation()
{
    if( navigator.geolocation )
    {
       // Call getCurrentPosition with success and failure callbacks
       navigator.geolocation.getCurrentPosition( success, fail );
    }
    else
    {
           alert("Sorry, your browser does not support geolocation services.");
    }
}

function success(position)
{

   document.getElementById('position').value = "Latitude: " + position.coords.latitude +   ", Longitude: " + position.coords.longitude;
}

function fail()
{
   // Could not obtain location
   document.getElementById('position').value = "Could not obtain location: Using Latitude: 0 and  Longitude: 0 ";
}
